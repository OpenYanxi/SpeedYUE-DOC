SpeedYUE - PHP框架
==============

SpeedPHP是一款轻量级框架，经过千万量级访问考验。

SpeedYUE为格悦定制版本，进行了定制优化和修改。

Git下载： http://git.oschina.net/OpenYanxi/SpeedYUE

文档查看地址：http://speedyue.book.yanxishe.cc  

文档源码地址：http://git.oschina.net/OpenYanxi/SpeedYUE-DOC

------------------

主要修改：

1. 功能调整

	* 增加Pathinfo获取参数的方式（可选关闭），可通过 /main/index/id/1 获取id为1的值
	
	* 全局增加网站根目录的静态相对地址 WEB_ROOT 常量，支持根目录或者子目录部署，可以直接在模板文件里写<?=WEB_ROOT?>


2. 目录和结构调整

	* i目录修改为 static目录
	
	* 全局将view文件修改为php文件，并增加防护读取函数
    
3. 模块开发默认首页，增加了路由

<m>  =>   <m>/main/index

即在Controller里新建文件夹，新建模块，URL地址里访问   http://xx.com/模块  >>即访问 Controller 文件夹下，该模块的 main index方法。


------------------

SpeedPHP原版
--

官网 http://www.speedphp.com/forum.php 

Github  https://github.com/SpeedPHP/speed

手册https://github.com/SpeedPHP/manual 